# Calculadora de notas
- calculadora de notas desenvolvida em React.JS, para alunos EAD-FADERGS, mostrando o resultado de sua média por disciplina e retornando uma mensagem de aprovação ou reprovação, se for o caso.

## React.JS, o que é ?

- O React é uma biblioteca JavaScript de código aberto com foco em criar interfaces de usuário em páginas web. É mantido pelo Facebook, Instagram, outras empresas e uma comunidade de desenvolvedores individuais. É utilizado nos sites da Netflix, Airbnb, iFood, Walmart, Uber, WhatsApp e obviamente Facebook, Instagram

- O seu principal objetivo é ser rápida, escalável e simples, podendo ainda ser usada em combinação com outras bibliotecas ou frameworks de JavaScript, como o Next JS

- React trabalha com o conceito de SPA (Single Page Aplication), com apenas um HTML se faz injeções de componentes, e é possível atualizar apenas um componente ou até uma parte desse componente sem precisar dar refresh na página

### comandos
- __start:__ Inicia o build no modo de desenvolvimento.
- __build:__ Executa o build do projeto otimizado para produção.
- __test:__ Executa os testes do projeto.
- __eject:__ Traz para dentro do nosso projeto, toda a configuração que o react-scripts abstrai.

### descrição de arquivos criados pelo CRA

- __package.json:__ é usado para armazenar os metadados associados ao projeto, bem como para armazenar a lista de pacotes de dependência

- __index.html:__ O arquivo index.html vem com a marcação mínima necessária para iniciar nossa aplicação. A tag div com o id 'root' será utilizada pelo React para renderizar nossa aplicação.

-  __src/App.js:__ contém o componente raiz da aplicação. Existem duas formas de definir componentes, através de functions ou através de class. Um componente deve sempre implementar um método render, que retorna um JSX do que deve ser mostrado na tela, ou null quando não deve mostrar nada.

- __index.js:__ ele que ocorre a inicialização da nossa aplicação. Importando o ReactDOM, o módulo do React responsável pela manipulação do DOM.

### Instalação

- npx create-react-app 'projectName'

## Colaborador

- Eduardo Vargas (Imbé-RS); Desenvolvedor Full-Stack; Graduando do curso de Gestão de TI - FADERGS - 2019/2021.