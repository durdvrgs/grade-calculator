import { useState } from 'react';
import './App.css';

import Calculator from './components/Calculator';
import Header from './components/Header';
import Welcome from './components/Welcome';


const App = () => {

  const [isStarted, setIsStarted] = useState(false)

  const icons = {
    'start': 'fas fa-play',
    'restart': 'fas fa-sync-alt',
    'calculate': 'fas fa-calculator'
  }
  const labels = [
    {name: 'A1'},
    {name: 'A2'},
    {name: 'A3'},
    {name: 'A4'},
    {name: 'N2'}
  ]

  return (
	<>
	<Header/>
	<main>
		{!isStarted && 
		<Welcome 
		thisAction={() => setIsStarted(true)}
		/>}

		{isStarted &&
		<Calculator
		thisLabels={labels}
		thisIcons={icons}
		thisAction={() => setIsStarted(false)}
		/>}
	</main>
	</>
  )
}

export default App;
