import "./style.css"


const Button = ({thisAction, thisText, thisIcon}) => {

    return (
        <div className="button-content">
            <button 
            onClick={thisAction}>
                <icon className={thisIcon}/>
                {thisText}
            </button>
        </div>
    )
}

export default Button