import "./style.css"
import { useState } from "react"

import Button from "../Button"
import Results from "../Results"
import TitleSubTitle from "../TitleSubTitle"



const Calculator = ({thisLabels, thisIcons}) => {

    //STATES
    const [results, setResults] = useState({
        n1: '',
        n2: '',
        media: ''
    })
    const [studentGrades, setStudentGrades] = useState({
        A1: '',
        A2: '',
        A3: '',
        A4: '',
        N2: '',
    })
    const [showResults, setShowResults] = useState(false)
    const rangeSpans = [0,1,2,3,4,5,6,7,8,9,10]

    //SERVICES
    const getGrades = (thisValue, thisName) => {

        let name = thisName
        let grade = thisValue
        // let gradeToFloat = parseFloat(grade)
        
        // const onlyIntegers = new RegExp('([0-9]*[.])?[0-9]')
        // let hasSomeLetter = grade.split('').some( (element) => !onlyIntegers.test(element) )
        
        // if ( hasSomeLetter ) {
        //     return setStudentGrades( { ...studentGrades, [name]: '' } )
        // }

        // if ( gradeToFloat > 10 ) {
        //     return setStudentGrades( { ...studentGrades, [name]: 10 } )
        // }

        return setStudentGrades( { ...studentGrades, [name]: grade } )
    }

    const calculateGrades = () => {

        let hasEmptyField = Object.values(studentGrades).some((grade) => grade === '')
        
        if ( hasEmptyField ) {
            return alert('Por favor, selecione uma nota para cada atividade.')
        }

        let n1GradesList = Object.values(studentGrades).slice(0, 4)

        let n1AtivitiesGrade = n1GradesList.reduce((acc, curr) => acc + curr, 0) / 4 * 0.4
        let n2AtivityGrade = studentGrades.N2 * 0.6
        let thisMedia = n1AtivitiesGrade + n2AtivityGrade

        setResults({
            n1: n1AtivitiesGrade.toFixed(2), 
            n2: n2AtivityGrade.toFixed(2), 
            media: thisMedia.toFixed(2)
        })

        setShowResults(true)
    }

    const resetCalculator = () => {

        setStudentGrades({
            A1: '',
            A2: '',
            A3: '',
            A4: '',
            N2: ''
        })

        setShowResults(false)
    }

    const setSpanLeftPercent = (index) => {

        if (index === 0) {
            return -.9
        }

        return index * 9.09
    }

    const setSpanColor = (index) => {

        if (index < 3) {
            return '#de3b3b'
        }
        else if (index < 6 && index >= 3) {
            return '#e46f51'
        }
        else if (index < 8 && index >= 6) {
            return '#d2d65a'
        }
        else {
            return '#5ad65a'
        }
    }

    return (
        <>
        <TitleSubTitle 
        thisH1={showResults ? 'Aqui está o Resultado'  : 'Calcule sua Média'}
        thisH3={showResults ? 'Confira o seu Desempenho...' : 'Selecione suas notas abaixo...'}
        />

        <div className="calculator-content">

            {!showResults &&
                <div className="inputs-content">

                    {thisLabels.map((label, index) => (
                        <div className="range-inputs-content" key={index}>

                            <label htmlFor={label.name}>
                                {"Nota " + label.name}
                            </label>

                            <input 
                            type="range"
                            name={label.name}
                            value={studentGrades[label.name]}
                            min="0"
                            max="10"
                            disabled
                            />

                            {rangeSpans.map((_, index) => (
                                <span
                                key={index}
                                className="grade-content"
                                id={index !== parseInt(studentGrades[label.name]) ? "range-span" : ""}
                                style={{"left": `${setSpanLeftPercent(index)}%`, "color": setSpanColor(index)}}
                                onClick={() => getGrades(index, label.name)}
                                >
                                    {index}
                                </span>
                            ))}


                        </div>

                        // <input
                        // key={index}
                        // name={label.name}
                        // maxLength={2}
                        // value={studentGrades[label.name]}
                        // placeholder={`Nota ${label.name}`}
                        // onChange={(e) => getGrades(e.target)}
                        // />
                    ))}
                </div>
            }

            {showResults &&
                <Results thisResults={results} />
            }

            <Button 
            thisText={showResults ? "Resetar" : "Calcular"}
            thisIcon={thisIcons[showResults ? 'restart' : 'calculate']}
            thisAction={() => showResults ? resetCalculator() :  calculateGrades()}
            />

        </div>
        </>
    )
}

export default Calculator