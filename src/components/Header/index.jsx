import logoFadergs from '../../images/logoFadergs.jpg'
import './style.css'


const Header = () => {

    return (
        <header>
            <div className="logo-content">
                <img src={logoFadergs} alt="logo da fadergs" />
            </div>
        </header>
    )
}

export default Header