import "./style.css"
import aprovado from "../../images/aprovado.png"
import reprovado from "../../images/reprovado.png"
import { useEffect, useRef, useState } from "react"


const Results = ({thisResults}) => {

    const thisLabels = ['Média-N1', 'Média-N2', 'Total']
    const resultSpan = useRef(null)
    const [ studentResult, setStudentResult ] = useState({
        label: '',
        emoji: ''
    })

    useEffect(() => {

        let isApproved = thisResults.media >= 6 ? true : false

        setStudentResult({
            label: isApproved ? 'Aprovado!' : 'Reprovado', 
            emoji: isApproved ? aprovado : reprovado
        })
        
        resultSpan.current.style.color = isApproved ? 'green' : 'red'

    }, [thisResults])


    return (
        <div className="results-content">

            {Object.values(thisResults)?.map((grade, index) => (
                <div className="results" key={index}>

                    <span>{thisLabels[index]}</span>
                    <span>{grade}</span>

                </div>
            ))}

            <div className="emoji-results-content">
                <img src={studentResult.emoji} alt="emoji reagindo ao desempenho do aluno"/>
                <span ref={resultSpan}>
                    {studentResult.label}
                </span>
            </div>

        </div>
    )
}

export default Results