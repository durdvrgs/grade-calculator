import './style.css'


const TitleSubTitle = ({thisH1, thisH3}) => {

    return (
        <div className="title-subtitle-content">
            <h1>{thisH1}</h1>
            <h3>{thisH3}</h3>
        </div>
    )
}

export default TitleSubTitle