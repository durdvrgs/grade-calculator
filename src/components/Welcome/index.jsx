import Button from "../Button"
import TitleSubTitle from "../TitleSubTitle"



const Welcome = ({thisAction}) => {

    return (
        <>
        <TitleSubTitle 
        thisH1={"Bem-vindo, estudante!"} 
        thisH3={"Calcule sua média e veja seu resultado aqui..."} 
        />

        <Button 
        thisAction={thisAction}
        thisText={'Iniciar'}
        thisIcon={'fas fa-play'}
        />
        </>
    )
}

export default Welcome